using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    Pelota pelota;
    public AudioSource source;
    public AudioClip GanarPerder;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        GanarPerder = GetComponent<AudioClip>();
    }
    void Update()
    {
        if (pelota.player1Score >= 10)
        {
            pelota.winP1.gameObject.SetActive(true);
            source.PlayOneShot(GanarPerder);
            pelota.Pause();
        }
        else if (pelota.player2Score >= 10)
        {
            pelota.winP1.gameObject.SetActive(true);
            source.PlayOneShot(GanarPerder);
            pelota.Pause();
        }
    }
}

using UnityEngine;

public class RandomColor : MonoBehaviour
{
    private Renderer rend;

    private void Start()
    {
        rend = GetComponent<Renderer>();
    }

    private void OnMouseDown()
    {
        CambiarColor();
    }

    private void CambiarColor()
    {
        Color nuevoColor = new(Random.value, Random.value, Random.value);
        rend.material.color = nuevoColor;
    }
}

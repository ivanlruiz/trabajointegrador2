using System.Collections;
using UnityEngine;

public class Velocidad : MonoBehaviour
{
    public float speedMultiplier = 1.2f;
    public float powerupDuration = 10f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            PlayerDetector playerDetector = other.GetComponent<PlayerDetector>();
            if (playerDetector != null)
            {
                int lastPlayerTouchedBall = playerDetector.lastPlayerWhoTouchedBall;

                GameObject playerObject = null;

                if (lastPlayerTouchedBall == playerDetector.player1ID)
                {
                    playerObject = GameObject.FindGameObjectWithTag("Player1");
                }
                else if (lastPlayerTouchedBall == playerDetector.player2ID)
                {
                    playerObject = GameObject.FindGameObjectWithTag("Player2");
                }

                if (playerObject != null)
                {
                    Movimiento playerMovement = playerObject.GetComponent<Movimiento>();
                    Movimiento2 playerMovement2 = playerObject.GetComponent<Movimiento2>();

                    if (playerMovement != null)
                    {
                        StartSpeeding(playerMovement);
                        StartCoroutine(SpeedCoroutine(playerMovement));
                        gameObject.transform.position = new Vector3(0, 100, 0); //fuera del mapa
                    }
                    else if (playerMovement2 != null)
                    {
                        StartSpeeding(playerMovement2);
                        StartCoroutine(SpeedCoroutine2(playerMovement2));
                        gameObject.transform.position = new Vector3(0, 100, 0); //fuera del mapa
                    }
                    else
                    {
                        Debug.LogError("No se encontr� el componente de movimiento en el jugador.");
                    }
                }
            }


        }
    }

    private IEnumerator SpeedCoroutine(Movimiento playerMovement)
    {
        float originalSpeed = playerMovement.moveSpeed;
        float targetSpeed = originalSpeed * speedMultiplier;

        playerMovement.moveSpeed = targetSpeed;

        yield return new WaitForSeconds(powerupDuration);

        playerMovement.moveSpeed = originalSpeed;
        Destroy(gameObject);
    }

    private IEnumerator SpeedCoroutine2(Movimiento2 playerMovement)
    {
        float originalSpeed = playerMovement.moveSpeed;
        float targetSpeed = originalSpeed * speedMultiplier;

        playerMovement.moveSpeed = targetSpeed;

        yield return new WaitForSeconds(powerupDuration);

        playerMovement.moveSpeed = originalSpeed;
        Destroy(gameObject);
    }

    private void StartSpeeding(Movimiento playerMovement)
    {
        Debug.Log("Acelerando el jugador " + playerMovement.tag);
    }

    private void StartSpeeding(Movimiento2 playerMovement)
    {
        Debug.Log("Acelerando el jugador " + playerMovement.tag);
    }
}

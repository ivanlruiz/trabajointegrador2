using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void Juego1()
    {
        SceneManager.LoadScene(1);
    }
    public void Juego2()
    {
        SceneManager.LoadScene(2);
    }
    public void Juego3()
    {
        SceneManager.LoadScene(3);
    }
    public void Juego4()
    {
        SceneManager.LoadScene(4);
    }
    public void Multi()
    {
        SceneManager.LoadScene(6);
    }

    public void BOT()
    {
        SceneManager.LoadScene(5);
    }
}

using UnityEngine;

public class Movimiento2 : MonoBehaviour
{
    public CharacterDatabase1 characterDB;
    public SpriteRenderer artw;
    private int selectedOption1 = 0;



    public float moveSpeed = 5f;
    public float jumpForce = 5f;
    public bool isJumping = false;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (!PlayerPrefs.HasKey("selectedOption1"))
        {
            selectedOption1 = 0;
        }
        else
        {
            Load1();
        }
        UpdateCharacter1(selectedOption1);
    }

    private void Update()
    {
        float moveX = 0f;

        // Movimiento con las flechas izquierda y derecha del teclado (Jugador 2)
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveX = -1f;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            moveX = 1f;
        }

        rb.velocity = new Vector2(moveX * moveSpeed, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.UpArrow) && !isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode.Impulse);
            isJumping = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
        if (collision.gameObject.CompareTag("Suelo"))
        {
            isJumping = false;
        }
    }

    private void UpdateCharacter1(int selectedOption1)
    {
        Character1 character1 = characterDB.GetCharacter1(selectedOption1);
        artw.sprite = character1.characterSprite;
    }

    void Load1()
    {
        selectedOption1 = PlayerPrefs.GetInt("selectedOption1");
    }
}

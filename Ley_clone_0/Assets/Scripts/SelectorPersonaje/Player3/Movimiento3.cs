using UnityEngine;

public class Movimiento3 : MonoBehaviour
{
    public CharacterDatabase2 characterDB;
    public SpriteRenderer artw;
    private int selectedOption3 = 0;



    public float moveSpeed = 5f;
    public float jumpForce = 5f;
    public bool isJumping = false;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (!PlayerPrefs.HasKey("selectedOption2"))
        {
            selectedOption3 = 0;
        }
        else
        {
            Load2();
        }
        UpdateCharacter2(selectedOption3);
    }

    private void Update()
    {
        float moveX = 0f;

        // Movimiento con las flechas izquierda y derecha del teclado (Jugador 2)
        if (Input.GetKey(KeyCode.J))
        {
            moveX = -1f;
        }
        else if (Input.GetKey(KeyCode.L))
        {
            moveX = 1f;
        }

        rb.velocity = new Vector2(moveX * moveSpeed, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.I) && !isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode.Impulse);
            isJumping = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }

    private void UpdateCharacter2(int selectedOption1)
    {
        Character2 character1 = characterDB.GetCharacter2(selectedOption1);
        artw.sprite = character1.characterSprite;
    }

    void Load2()
    {
        selectedOption3 = PlayerPrefs.GetInt("selectedOption2");
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterDatabase3 : ScriptableObject
{
    public Character3[] character;

    public int CharacterCount
    {
        get { return character.Length; }
    }

    public Character3 GetCharacter3(int index)
    {
        return character[index];
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    public int player1ID = 1;
    public int player2ID = 2;

    public int lastPlayerWhoTouchedBall;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            lastPlayerWhoTouchedBall = player1ID;
        }
        else if (collision.gameObject.CompareTag("Player2"))
        {
            lastPlayerWhoTouchedBall = player2ID;
        }
    }

    void Update()
    {
        if (lastPlayerWhoTouchedBall == player1ID)
        {
            // Acciones específicas para el jugador 1
        }
        else if (lastPlayerWhoTouchedBall == player2ID)
        {
            // Acciones específicas para el jugador 2
        }
    }
}
    
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] powerUps;  // Array de PowerUps disponibles
    private float minX = -7f;
    private float maxX = 7f;
    private float minY = 2f;
    private float maxY = 3f;
    public float spawnInterval = 15f;

    private void Start()
    {
        InvokeRepeating("InstantiatePowerUp", spawnInterval, spawnInterval);
    }

    private void InstantiatePowerUp()
    {
        // Elige aleatoriamente un PowerUp del array
        GameObject powerUp = powerUps[Random.Range(0, powerUps.Length)];

        // Genera coordenadas aleatorias dentro de los rangos especificados
        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);
        float z = 0f;

        // Instancia el PowerUp en las coordenadas generadas
        Instantiate(powerUp, new Vector3(x, y, z), Quaternion.identity);
    }
}

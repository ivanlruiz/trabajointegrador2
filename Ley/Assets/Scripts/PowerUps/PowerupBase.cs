using UnityEngine;

public abstract class PowerupBase : MonoBehaviour
{
    protected bool powerupActive = false;
    protected GameObject lastPlayerTouched;

    protected void ActivatePowerup(GameObject player)
    {
        lastPlayerTouched = player;
        powerupActive = true;
        // Implementar cualquier l�gica adicional com�n para activar el powerup aqu�
        // Por ejemplo, puedes mostrar alg�n efecto visual o reproducir un sonido.
    }

    protected void DeactivatePowerup()
    {
        powerupActive = false;
        lastPlayerTouched = null;
        // Implementar cualquier l�gica adicional com�n para desactivar el powerup aqu�
    }
}

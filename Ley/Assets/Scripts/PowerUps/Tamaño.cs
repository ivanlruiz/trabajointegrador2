using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tamaño : MonoBehaviour
{
    public float factorEscala = 2f;
    public float powerupDuration = 5f;
    private bool active = false;

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Colisión detectada con " + other.tag);

        if (other.CompareTag("Ball"))
        {
            PlayerDetector playerDetector = other.GetComponent<PlayerDetector>();

            if (playerDetector == null)
            {
                Debug.LogError("El componente PlayerDetector no está adjunto correctamente al objeto 'Ball'.");
                return;
            }

            int lastPlayerWhoTouchedBall = playerDetector.lastPlayerWhoTouchedBall;

            GameObject player = null;

            if (lastPlayerWhoTouchedBall == playerDetector.player1ID)
            {
                player = GameObject.FindGameObjectWithTag("Player1");
            }
            else if (lastPlayerWhoTouchedBall == playerDetector.player2ID)
            {
                player = GameObject.FindGameObjectWithTag("Player2");
            }

            if (player != null)
            {
                active = true;
                Debug.Log("Aumentando la escala del jugador " + player.tag);
                player.transform.localScale *= factorEscala;
                StartCoroutine(PowerupCoroutine(player));
                gameObject.transform.position = new Vector3(0, 100, 0); //fuera del mapa
            }
        }
    }

    public IEnumerator PowerupCoroutine(GameObject player)
    {
        Debug.Log("Empezando");
        yield return new WaitForSeconds(powerupDuration);
        player.transform.localScale /= factorEscala;
        Destroy(gameObject);
    }
}

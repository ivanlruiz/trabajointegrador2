using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Pelota : MonoBehaviour
{
    public int player1Score = 0;
    public int player2Score = 0;

    public AudioSource AudioSource;
    public AudioClip WinAudio;

    public TextMeshProUGUI player1ScoreText;
    public TextMeshProUGUI player2ScoreText;

    public TextMeshProUGUI winP1;
    public TextMeshProUGUI winP2;

    public Transform startPos;
    public float initialForce = 5f;

    Rigidbody rb;

    private void Start()
    {
        AudioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
    }
    public void Pause()
    {
        Time.timeScale = 0;
    }
    private void Update()
    {
        if (player1Score >= 10)
        {
            winP1.gameObject.SetActive(true);
            
            Pause();

        }
        else if (player2Score >= 10)
        {
            winP2.gameObject.SetActive(true);

            Pause();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            if (collision.gameObject.name == "Ground1")
            {
                player2Score++;
            }
            else if (collision.gameObject.name == "Ground2")
            {
                player1Score++;
            }

            ResetPos();
        }
    }

    private void ResetPos()
    {
        transform.position = startPos.position;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = Vector3.zero;

        Vector2 direction;
        if (player1Score > player2Score)
        {
            direction = Vector2.left;
        }
        else
        {
            direction = Vector2.right;
        }

        rb.AddForce(direction * initialForce, ForceMode.Impulse);

        player1ScoreText.text = player1Score.ToString();
        player2ScoreText.text = player2Score.ToString();
    }
}
using UnityEngine;
using Unity.Netcode;
public class Movimiento : NetworkBehaviour
{
    
    public CharacterDatabase characterDB;
    public SpriteRenderer artw;
    private int selectedOption = 0;



    public float moveSpeed;
    public float jumpForce;
    public bool isJumping = false;

    private Rigidbody rb;

    public override void OnNetworkSpawn()
    {
        if (!IsOwner) return;
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (!PlayerPrefs.HasKey("selectedOption"))
        {
            selectedOption = 0;
        }
        else
        {
            Load();
        }
        UpdateCharacter(selectedOption);
    }

    private void Update()
    {
        float moveX = 0f;

        if (Input.GetKey(KeyCode.A))
        {
            moveX = -1f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            moveX = 1f;
        }

        rb.velocity = new Vector2(moveX * moveSpeed, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.W) && !isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode.Impulse);
            isJumping = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
        if (collision.gameObject.CompareTag("Suelo"))
        {
            isJumping = false;
        }
    }


    private void UpdateCharacter(int selectedOption)
    {
        Character character = characterDB.GetCharacter(selectedOption);
        artw.sprite = character.characterSprite;
    }

    void Load()
    {
        selectedOption = PlayerPrefs.GetInt("selectedOption");
    }
}

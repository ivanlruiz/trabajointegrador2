using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterManager2 : MonoBehaviour
{
    public CharacterDatabase2 characterDB;

    public SpriteRenderer artw;

    private int selectedOption2 = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("selectedOption2"))
        {
            selectedOption2 = 0;
        }
        else
        {
            Load2();
        }
        UpdateCharacter2(selectedOption2);
    }

    public void NextOption2()
    {
        selectedOption2++;

        if (selectedOption2 >= characterDB.CharacterCount)
        {
            selectedOption2 = 0;
        }

        UpdateCharacter2(selectedOption2);
        Save2();
    }


    public void BackOption2()
    {
        selectedOption2--;

        if (selectedOption2 < 0)
        {
            selectedOption2 = characterDB.CharacterCount - 1;
        }
        UpdateCharacter2(selectedOption2);
        Save2();
    }
    private void UpdateCharacter2(int selectedOption)
    {
        Character2 character2 = characterDB.GetCharacter2(selectedOption);
        artw.sprite = character2.characterSprite;
    }

    void Load2()
    {
        selectedOption2 = PlayerPrefs.GetInt("selectedOption2");
    }

    void Save2()
    {
        PlayerPrefs.SetInt("selectedOption2", selectedOption2);
    }


}

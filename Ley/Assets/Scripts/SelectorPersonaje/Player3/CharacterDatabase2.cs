using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterDatabase2 : ScriptableObject
{
    public Character2[] character;

    public int CharacterCount
    {
        get { return character.Length; }
    }

    public Character2 GetCharacter2(int index)
    {
        return character[index];
    }
}

using UnityEngine;

public class Movimiento4 : MonoBehaviour
{
    public CharacterDatabase3 characterDB;
    public SpriteRenderer artw;
    private int selectedOption3 = 0;



    public float moveSpeed = 5f;
    public float jumpForce = 5f;
    public bool isJumping = false;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (!PlayerPrefs.HasKey("selectedOption3"))
        {
            selectedOption3 = 0;
        }
        else
        {
            Load3();
        }
        UpdateCharacter3(selectedOption3);
    }

    private void Update()
    {
        float moveX = 0f;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveX = -1f;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            moveX = 1f;
        }

        rb.velocity = new Vector2(moveX * moveSpeed, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.UpArrow) && !isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode.Impulse);
            isJumping = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }

    private void UpdateCharacter3(int selectedOption1)
    {
        Character3 character1 = characterDB.GetCharacter3(selectedOption1);
        artw.sprite = character1.characterSprite;
    }

    void Load3()
    {
        selectedOption3 = PlayerPrefs.GetInt("selectedOption3");
    }
}

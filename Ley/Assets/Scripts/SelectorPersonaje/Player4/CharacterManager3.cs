using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterManager3 : MonoBehaviour
{
    public CharacterDatabase3 characterDB;

    public SpriteRenderer artw;

    private int selectedOption3 = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("selectedOption3"))
        {
            selectedOption3 = 0;
        }
        else
        {
            Load3();
        }
        UpdateCharacter3(selectedOption3);
    }

    public void NextOption3()
    {
        selectedOption3++;

        if (selectedOption3 >= characterDB.CharacterCount)
        {
            selectedOption3 = 0;
        }

        UpdateCharacter3(selectedOption3);
        Save3();
    }


    public void BackOption3()
    {
        selectedOption3--;

        if (selectedOption3 < 0)
        {
            selectedOption3 = characterDB.CharacterCount - 1;
        }
        UpdateCharacter3(selectedOption3);
        Save3();
    }
    private void UpdateCharacter3(int selectedOption)
    {
        Character3 character3 = characterDB.GetCharacter3(selectedOption);
        artw.sprite = character3.characterSprite;
    }

    void Load3()
    {
        selectedOption3 = PlayerPrefs.GetInt("selectedOption3");
    }

    void Save3()
    {
        PlayerPrefs.SetInt("selectedOption3", selectedOption3);
    }


}

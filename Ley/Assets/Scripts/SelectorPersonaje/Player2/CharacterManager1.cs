using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterManager1 : MonoBehaviour
{
    public CharacterDatabase1 characterDB;

    public SpriteRenderer artw;

    private int selectedOption1 = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("selectedOption1"))
        {
            selectedOption1 = 0;
        }
        else
        {
            Load1();
        }
        UpdateCharacter1(selectedOption1);
    }

    public void NextOption1()
    {
        selectedOption1++;

        if (selectedOption1 >= characterDB.CharacterCount)
        {
            selectedOption1 = 0;
        }

        UpdateCharacter1(selectedOption1);
        Save1();
    }


    public void BackOption1()
    {
        selectedOption1--;

        if (selectedOption1 < 0)
        {
            selectedOption1 = characterDB.CharacterCount - 1;
        }
        UpdateCharacter1(selectedOption1);
        Save1();
    }
    private void UpdateCharacter1(int selectedOption)
    {
        Character1 character1 = characterDB.GetCharacter1(selectedOption);
        artw.sprite = character1.characterSprite;
    }

    void Load1()
    {
        selectedOption1 = PlayerPrefs.GetInt("selectedOption1");
    }

    void Save1()
    {
        PlayerPrefs.SetInt("selectedOption1", selectedOption1);
    }


}

using UnityEngine;

public enum BotState
{
    SearchBall,
    GoToBall,
    Wait
}

public class BotController : MonoBehaviour
{
    public float botSpeed;
    public float jumpForce;
    public float jumpCooldown = 1.0f;
    public float predictionTime = 0.7f;

    private float lastJumpTime;
    private bool isJumping = false;

    private Rigidbody rb;
    private Transform ballTransform;
    private BotState currentState;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        // Find the position of the ball in the scene
        ballTransform = GameObject.FindWithTag("Pelota").transform;

        // Set initial state
        currentState = BotState.SearchBall;
    }

    private void Update()
    {
        bool shouldJump = ShouldJump();

        if (shouldJump && !isJumping && Time.time > lastJumpTime + jumpCooldown)
        {
            Jump();
        }

        switch (currentState)
        {
            case BotState.SearchBall:
                if (ballTransform.position.x > 0f)
                {
                    // Switch to "GoToBall" state
                    currentState = BotState.GoToBall;
                }
                // You can add more conditions or logic here if needed
                break;
            case BotState.GoToBall:
                if (ballTransform.position.x <= 0f)
                {
                    // Switch to "Wait" state
                    currentState = BotState.Wait;
                }
                // Logic to move towards the ball
                GoToBall();
                break;
            case BotState.Wait:
                if (ballTransform.position.x > 0f)
                {
                    // Switch to "SearchBall" state
                    currentState = BotState.SearchBall;
                }
                // Logic for waiting
                Wait();
                break;
        }
    }

    private bool ShouldJump()
    {
        // Calculate the direction and relative distance between the bot and the ball
        Vector3 relativeDirection = ballTransform.position - transform.position;
        float distance = relativeDirection.magnitude;
        relativeDirection.Normalize();

        // Calculate the relative height of the ball compared to the bot
        float relativeHeight = ballTransform.position.y - transform.position.y;

        // Calculate the relative velocity of the ball compared to the bot
        Vector3 relativeVelocity = ballTransform.GetComponent<Rigidbody>().velocity - rb.velocity;

        // Define jump conditions based on position, velocity, and other factors
        if ((relativeDirection.y > 0f && Mathf.Abs(relativeDirection.x) < 2f && distance < 5f && relativeVelocity.y > 0f) ||
            (relativeHeight <= -0.5f))
        {
            return true;
        }

        return false;
    }

    private void Jump()
    {
        rb.AddForce(new Vector3(0f, jumpForce, 0f), ForceMode.Impulse);
        isJumping = true;
        lastJumpTime = Time.time;
    }

    private void GoToBall()
    {
        // Predict the position of the ball in the future
        Vector3 predictedPosition = PredictBallPosition(predictionTime);

        // Calculate the direction towards the predicted position of the ball
        Vector3 targetDirection = predictedPosition - transform.position;
        targetDirection.Normalize();

        // Use Seek algorithm to move smoothly towards the predicted position of the ball
        Vector3 targetVelocity = targetDirection * botSpeed;
        rb.velocity = new Vector3(targetVelocity.x, rb.velocity.y, 0f);
    }

    private void Wait()
    {
        rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }

    private Vector3 PredictBallPosition(float time)
    {
        Vector3 ballPosition = ballTransform.position;
        Vector3 ballVelocity = ballTransform.GetComponent<Rigidbody>().velocity;

        // Predict the position of the ball after 'time' seconds assuming constant velocity
        Vector3 predictedPosition = ballPosition + ballVelocity * time;

        return predictedPosition;
    }
}
